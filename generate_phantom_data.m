% generate_phantom_data.m
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>

FOV = 2.5; % FOV of the shepp-logan phantom, FOV >= 2 regulates the scale;
oFE = 2;

% generate 2d-CARTESIAN data
disp('generating CARTESIAN data')
X = 256 * oFE;
Y = 256;
k = knopp_CARTESIAN(X, Y);
k = reshape(k, [], 2);
f = phantom_k(...
    k(:, 1) * X / FOV / oFE, ...
    k(:, 2) * Y / FOV);
base_fname = 'phantom_CARTESIAN';
fname = strcat(base_fname, sprintf('_FOV_%.1f_%dx%d', FOV, X, Y));
fname = strcat(fname, '_data.dat');
[fid, err] = fopen(fname, 'w');
fwrite(fid, single([real(f(:)), imag(f(:))]'), 'float32');
fclose(fid);
fname = strcat(base_fname, sprintf('_FOV_%.1f_%dx%d', FOV, X, Y));
fname = strcat(fname, '_knots.dat');
[fid, err] = fopen(fname, 'w');
fwrite(fid, k, 'float32');
fclose(fid);

% generate 2d-RADIAL data
disp('generating RADIAL data')
R = 256 * oFE;
P = 256;
k = knopp_RADIAL(R, P);
k = reshape(k, [], 2);
f = phantom_k(...
    k(:, 1) * R / FOV / oFE, ...
    k(:, 2) * R / FOV / oFE);
base_fname = 'phantom_RADIAL';
fname = strcat(base_fname, sprintf('_FOV_%.1f_%dx%d', FOV, R, P));
fname = strcat(fname, '_data.dat');
[fid, err] = fopen(fname, 'w');
fwrite(fid, single([real(f(:)), imag(f(:))]'), 'float32');
fclose(fid);
fname = strcat(base_fname, sprintf('_FOV_%.1f_%dx%d', FOV, R, P));
fname = strcat(fname, '_knots.dat');
[fid, err] = fopen(fname, 'w');
fwrite(fid, k, 'float32');
fclose(fid);

% generate reference data
disp('generating reference data')
X = 256;
Y = 256;
k = knopp_CARTESIAN(X, Y);
k = reshape(k, [], 2);
f_hat = phantom_i(...
    k(:, 1) * FOV, ...
    k(:, 2) * FOV);
base_fname = 'phantom_reference';
fname = strcat(base_fname, sprintf('_FOV_%.1f_%dx%d', FOV, X, Y));
fname = strcat(fname, '.dat');
[fid, err] = fopen(fname, 'w');
fwrite(fid, f_hat, 'float32');
fclose(fid);