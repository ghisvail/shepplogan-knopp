% test_knopp.m

plt2d = figure();

% 2d-RADIAL
R = 32;
P = 32;
k = knopp_RADIAL(R, P);

subplot(131);
k = reshape(k, [], 2);
plot(k(:, 1), k(:, 2), 'bo-')
axis equal; title('RADIAL')

% 2d-RADIAL, with EPI readout
R = 32;
P = 32;
k = knopp_RADIAL(R, P, 'epi');

subplot(132);
k = reshape(k, [], 2);
plot(k(:, 1), k(:, 2), 'bo-')
axis equal; title('RADIAL EPI')

% 2d-SPIRAL
M = 256;
w = 8;
k = knopp_SPIRAL(M, w);

subplot(133);
k = reshape(k, [], 2);
plot(k(:, 1), k(:, 2), 'bo-')
axis equal; title('SPIRAL')

plt3d = figure();

% 3d-RADIAL
R = 16;
P = 16;                                                                        
Q = 16;
k = knopp_3d_RADIAL(R, P, Q);

subplot(131)
k = reshape(k, [], 3);
plot3(k(:, 1), k(:, 2), k(:, 3), 'bo-')
title('3d-RADIAL')

% 2d+1-RADIAL
R = 16;                                                                        
P = 16;
S = 4;
k = knopp_stack_of_RADIAL(R, P, S);

subplot(132)
k = reshape(k, [], 3);
plot3(k(:, 1), k(:, 2), k(:, 3), 'bo-')
title('2d+1-RADIAL')

% 2d+1-SPIRAL
M = 256;                                                                        
w = 8;
S = 4;
k = knopp_stack_of_SPIRAL(M, w, S);

subplot(133)
k = reshape(k, [], 3);
plot3(k(:, 1), k(:, 2), k(:, 3), 'bo-')
title('2d+1-SPIRAL')