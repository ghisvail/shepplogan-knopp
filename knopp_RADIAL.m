function k = knopp_RADIAL(R, P, varargin)
% function k = Knopp_RADIAL(R, P, varargin)
%
% Description
% -----------
% Generates a vector of Cartesian coordinates corrsponding to a uniform radial
% distribution.
%
% Parameters
% ----------
% R: number of samples per spoke.
% P: number of radial spokes.
%
% Returns
% -------
% k: trajectory normalized in [-0.5; 0.5]. A vector ordered as
% [[kx_0, ..., kx_{n-1}], [ky_0, ..., ky_{n-1}]]
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>

r = 0:R-1;
p = 0:P-1;

epi = 0;
if ~isempty(varargin)
    for iArg=1:length(varargin)
        switch varargin{iArg}
            case {'epi'}
                epi = 1;
        end
    end
end

theta_p = p * pi / P;
if epi
	theta_p = theta_p + (pi / 2) * (1 - power(-1, p)); 
end

k = (r / R - 0.5)' * [cos(theta_p), sin(theta_p)];
k = k(:);
