function k = knopp_stack_of_SPIRAL(M, w, S)
% function k = knopp_stack_of_SPIRAL(M, w, S)
%
% Description
% -----------
% Generates a vector of Cartesian coordinates corrsponding to a stack of
% spiral distribution.
%
% Parameters
% ----------
% M: number of samples along the readout of the spiral
% w: number of swirls composing the spiral. Default to M^{1/2}*0.78, if no
% value is provided, which produces a constant velocity spiral.
% S: stack size
%
% Returns
% -------
% k: trajectory normalized in [-0.5; 0.5]. A vector ordered as
% [[kx_0, ..., kx_{n-1}], [kz_0, ..., kz_{n-1}], [kz_0, ..., kz_{n-1}]]
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>

k = knopp_stack_of_generic(S, @knopp_SPIRAL, {M, w});
