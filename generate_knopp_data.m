% generate_knopp_data.m
%
% Script for generating the Shepp-Logan raw data used in Knopp et al., "A 
% note on the iterative MRI reconstruction from nonuniform k-space data."
% and presented at http://www-user.tu-chemnitz.de/~potts/nfft/mri.php.
%
% This script requires the shepplogan tools from Ghislain Vaillant 
% available at https://ghisvail@bitbucket.org/ghisvail/shepplogan.git.
%
% Then make sure your MATLAB path is set up appropriately:
% 1) open a MATLAB prompt
% 2) cd <path_to_cloned_shepplogan_toolbox_repository>
% 3) addpath(pwd)
% 4) cd jar/
% 5) javaaddpath(fullfile(pwd, 'STBB.jar'))
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>

FOV = 2; % FOV of the shepp-logan phantom, FOV >= 2 regulates the scale;

% generate 3d-CARTESIAN data
disp('generating 3d-CARTESIAN data')
X = 256;
Y = 256;
Z = 36;
k = knopp_3d_CARTESIAN(X, Y, Z);
k = reshape(k, [], 3);
f = phantom3_k(k(:, 1) * X / FOV, ...
    k(:, 2) * Y / FOV, ...
    k(:, 3) * Z / FOV);
base_fname = 'knopp_3d_CARTESIAN';
fname = strcat(base_fname, sprintf('_%dx%dx%d', X, Y, Z));
fname = strcat(fname, '_data.dat');
[fid, err] = fopen(fname, 'w');
fwrite(fid, single([real(f(:)), imag(f(:))]'), 'float32');
fclose(fid);
fname = strcat(base_fname, sprintf('_%dx%dx%d', X, Y, Z));
fname = strcat(fname, '_knots.dat');
[fid, err] = fopen(fname, 'w');
fwrite(fid, k, 'float32');
fclose(fid);

% generate 3d-RADIAL data
disp('generating 3d-RADIAL data')
P = 160;
Q = 160;
R = 160;
k = knopp_3d_RADIAL(R, P, Q);
k = reshape(k, [], 3);
f = phantom3_k(k(:, 1) * R / (FOV / 2), ...
    k(:, 2) * R / (FOV / 2), ...
    k(:, 3) * R / (FOV / 2));
base_fname = 'knopp_3d_RADIAL';
fname = strcat(base_fname, sprintf('_%dx%dx%d', R, P, Q));
fname = strcat(fname, '_data.dat');
[fid, err] = fopen(fname, 'w');
fwrite(fid, single([real(f(:)), imag(f(:))]'), 'float32');
fclose(fid);
fname = strcat(base_fname, sprintf('_%dx%dx%d', R, P, Q));
fname = strcat(fname, '_knots.dat');
[fid, err] = fopen(fname, 'w');
fwrite(fid, k, 'float32');
fclose(fid);

% generate 2d+1-RADIAL data
disp('generating RADIAL data')
P = 256;
R = 256;
S = 36;
k = knopp_stack_of_RADIAL(R, P, S);
k = reshape(k, [], 3);
f = phantom3_k(k(:, 1) * R / FOV, ...
    k(:, 2) * R / FOV, ...
    k(:, 3) * S / FOV);
base_fname = 'knopp_RADIAL';
fname = strcat(base_fname, sprintf('_%dx%dx%d', R, P, S));
fname = strcat(fname, '_data.dat');
[fid, err] = fopen(fname, 'w');
fwrite(fid, single([real(f(:)), imag(f(:))]'), 'float32');
fclose(fid);
fname = strcat(base_fname, sprintf('_%dx%dx%d', R, P, S));
fname = strcat(fname, '_knots.dat');
[fid, err] = fopen(fname, 'w');
fwrite(fid, k, 'float32');
fclose(fid);

% generate 2d+1-SPIRAL data
disp('generating SPIRAL data')
M = 65536;
w = round(0.78125 * sqrt(M));
S = 36;
k = knopp_stack_of_SPIRAL(M, w, S);
k = reshape(k, [], 3);
f = phantom3_k(k(:, 1) * w / (FOV / 2), ...
    k(:, 2) * w / (FOV / 2), ...
    k(:, 3) * S / FOV);
base_fname = 'knopp_SPIRAL';
fname = strcat(base_fname, sprintf('_%dx%d', M, S));
fname = strcat(fname, '_data.dat');
[fid, err] = fopen(fname, 'w');
fwrite(fid, single([real(f(:)), imag(f(:))]'), 'float32');
fclose(fid);
fname = strcat(base_fname, sprintf('_%dx%d', M, S));
fname = strcat(fname, '_knots.dat');
[fid, err] = fopen(fname, 'w');
fwrite(fid, k, 'float32');
fclose(fid);