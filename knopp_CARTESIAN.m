function [k] = knopp_CARTESIAN(X, Y)
%function [k] = Knopp_CARTESIAN(X, Y)
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>

x = (0:X-1) / X - 0.5;
x = repmat(reshape(x, X, 1), [1 Y]);
y = (0:Y-1) / Y - 0.5;
y = repmat(reshape(y, 1, Y), [X 1]);
k = [x(:), y(:)];
k = k(:);
