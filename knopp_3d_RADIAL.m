function k = knopp_3d_RADIAL(R, P, Q)
% function k = knopp_3d_RADIAL(R, P, Q)
%
% Description
% -----------
% Generates a vector of Cartesian coordinates corrsponding to a uniform radial
% distribution in 3D.
%
% Parameters
% ----------
% R: number of samples per spoke.
% P, Q: number of radial spokes.
%
% Returns
% -------
% k: trajectory normalized in [-0.5; 0.5]. A vector ordered as
% [[kx_0, ..., kx_{n-1}], [ky_0, ..., ky_{n-1}], [kz_0, ..., kz_{n-1}]]
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>

r = 0:R-1;
p = 0:P-1;
q = 0:Q-1;

phi_p = 2 * pi * p / P;
theta_q = pi * (2 * q + 1) / (2 * Q);
cos_phi_p_sin_theta_q = cos(phi_p)' * sin(theta_q);
sin_phi_p_sin_theta_q = sin(phi_p)' * sin(theta_q);
cos_theta_q = ones(P, 1) * cos(theta_q);

k = (((r + 1) / (sqrt(2) * R))' * ...
    [cos_phi_p_sin_theta_q(:)', sin_phi_p_sin_theta_q(:)', cos_theta_q(:)']);
k = k(:);
