function k = knopp_stack_of_RADIAL(R, P, S, varargin)
% function k = knopp_stack_of_RADIAL(R, P, S, {alt,})
%
% Description
% -----------
% Generates a vector of Cartesian coordinates corrsponding to a stack of
% radial distribution.
%
% Parameters
% ----------
% R: number of samples per spoke.
% P: number of radial spokes.
% S: stack size
% alt: optional, whether to make odd and even spokes alternate between [0, \pi)
% and [\pi, 2\pi) as done by the scanner. Disabled by default (0).
%
% Returns
% -------
% k: trajectory normalized in [-0.5; 0.5]. A vector ordered as
% [[kx_0, ..., kx_{n-1}], [kz_0, ..., kz_{n-1}], [kz_0, ..., kz_{n-1}]]
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>

k = knopp_stack_of_generic(S, @knopp_RADIAL, {R, P, varargin{:}});
