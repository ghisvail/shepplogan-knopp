function k = knopp_stack_of_generic(S, func, func_args, varargin)
% function k = knopp_stack_of_generic(S, func, func_args, varargin)
%
% Description
% -----------
% S: size of stack
% func: handle to 2-D trajectory function to be stacked
% func_args: contains the arguments to func
%
% Note
% ----
% This function is used to factor out code for other stack_of_<trajectory>
% functions and is not meant for direct usage.

try
	k = func(func_args{:});
catch
	error('RuntimeError: error while calling handled function')	
end

k = reshape(k, [], 2);
N = size(k, 1);
kz = ones(N, 1) * [(0:S-1)/S-0.5];
k = [repmat(k, [S, 1])  kz(:)];
k = k(:);
