function [k] = knopp_3d_CARTESIAN(X, Y, Z)
%function [k] = Knopp_3d_CARTESIAN(nFE, nPE, nSE)
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>

x = (0:X-1) / X - 0.5;
x = repmat(reshape(x, X, 1, 1), [1 Y, Z]);
y = (0:Y-1) / Y - 0.5;
y = repmat(reshape(y, 1, Y, 1), [X 1 Z]);
z = (0:Z-1) / Z - 0.5;
z = repmat(reshape(z, 1, 1, Z), [X Y 1]);
k = [x(:), y(:), z(:)];
k = k(:);
