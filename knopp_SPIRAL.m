function k = knopp_SPIRAL(M, w)
% function k = Knopp_SPIRAL(M, w)
%
% Description
% -----------
% Generates a vector of Cartesian coordinates corrsponding to a constant
% velocity Archimedean spiral.
% 
% Parameters
% ----------
% M: number of samples along the readout of the spiral
% w: number of swirls composing the spiral. Default to M^{1/2}*0.78, if no
% value is provided, which produces a constant velocity spiral.
%
% Returns
% -------
% k: trajectory normalized in [-0.5; 0.5], returned as a vector ordered as
% [(kx_0, ..., kx_{n-1}), (ky_0, ..., ky_{n-1})]
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>

if isempty(w)
	w = sqrt(M) * 0.78125; % constant velocity spiral
end

j = 0:M-1;
t = sqrt(j / M);
omega_t = 2 * pi * w * t;

k = repmat(0.5 * t, [1, 2]) .* [cos(omega_t) sin(omega_t)];
k = k(:);
